

function HamburgerException (message) {
    this.message = message;
    this.name = 'HamburgerException';
}

function Hamburger(size, stuffing) {
    try{
        if(!size){
            throw new HamburgerException("no size given");
        } else if (!stuffing){
            throw new HamburgerException("no stuffing given");
        } else{
            this.size = size;
            this.stuffing = stuffing;
            this.topping = [];
        }
    }catch (e) {
        console.error(e.name +": "+ e.message);
    }

}

Hamburger.SIZE_SMALL = {
    size:"small",
    price:50,
    kkal:20
};
Hamburger.SIZE_LARGE = {
    size:"large",
    price:100,
    kkal:40
};
Hamburger.STUFFING_CHEESE = {
    stuffing:"cheese",
    price:10,
    kkal:20
};
Hamburger.STUFFING_SALAD = {
    stuffing:"salad",
    price:20,
    kkal:5
};
Hamburger.STUFFING_POTATO = {
    stuffing:"potato",
    price:15,
    kkal:10
};
Hamburger.TOPPING_MAYO = {
    topping:"mayonese",
    price:20,
    kkal:5
};
Hamburger.TOPPING_SPICE = {
    topping:"spicy",
    price:15,
    kkal:0
};

Hamburger.prototype.addTopping = function (topping) {
    try{
        if(this.topping.includes(topping)){
            throw new HamburgerException("Этот топпинг уже добавлен")
        }else {
            this.topping.push(topping);
        }
    }catch (e){
        console.error(e.name +" "+ e.message);
    }
};



Hamburger.prototype.removeTopping = function (topping) {
    try{
        let index = this.topping.indexOf(topping);
        if(index === -1){
            throw new HamburgerException("Этого топпинга нет в списке добавленых")
        }
        this.topping.splice(index,1);
    }catch(e){
        console.error(e.name +": "+ e.message);
    }
};

Hamburger.prototype.getToppings = function () {
    try{
        if (this.topping.length == null){
            throw new HamburgerException("В вашем гамбургере отсутствуют топпинги!") ;
        } else {
            return this.topping;
        }
    }catch(e){
        console.error(e.name +": "+ e.message);
    }
};

Hamburger.prototype.getSize = function () {
    return this.size.size;
};

Hamburger.prototype.getStuffing = function () {
    return this.stuffing.stuffing;
};

Hamburger.prototype.calculatePrice = function () {
    let sum = 0;
    sum += this.size.price+ this.stuffing.price;
    this.topping.forEach((e) => {
        sum += e.price;
    })
    return "Цена за гамбургер = " + sum +" грн.";
};

Hamburger.prototype.calculateKkal = function () {
    let sum = 0;
    sum += this.size.kkal +this.stuffing.kkal;
    this.topping.forEach((e)=>{
        sum+=e.kkal;
    });
    return "Суммарное количество калорий " +sum + " kkal";
};


let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.calculateKkal());
