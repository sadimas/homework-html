class Trello {
    constructor(classList,id){
        this.classList = classList;
        this.id = id;
        this.elem = null;
        this.drugElem = null;
    }

    render(){
        this.elem=document.createElement("div");
        let  {elem} = this;
        elem.className = this.classList;
        elem.id = this.id;
        let text = document.createElement("h2");
        text.textContent = "To do list";
        elem.append(text);
        let sortBtn = document.createElement("span");
        sortBtn.className='sort';
        sortBtn.textContent='Sort';
        sortBtn.addEventListener("click",this.sort.bind(this));
        elem.append(sortBtn);
        let taskEdit = document.createElement("button");
        taskEdit.textContent='Add a card...';
        taskEdit.className="task-edit active";
        taskEdit.addEventListener("click",this.buttonRender.bind(this));
        elem.append(taskEdit);
        elem.addEventListener("dragover",this.dragOver.bind(this));
        elem.addEventListener("drop",this.drop.bind(this));
        document.body.append(elem);
        return elem;

    }

    sort(){
        let list = Array.from(document.querySelectorAll(".task"));
        list.sort((a,b)=>a.innerText>b.innerText?1:-1)
            .map(node=>this.elem.appendChild(node))
    }

    buttonRender(e){
        let closeBtn = document.createElement("span");
        closeBtn.textContent="x";
        closeBtn.className='close-button active';
        this.elem.append(closeBtn);
        let textArea = document.createElement("textarea");
        textArea.placeholder='Введите текст';
        textArea.className='text-area active';
        this.elem.append(textArea);
        e.target.classList.remove("active");
        let button = document.createElement("button");
        button.className="task-button active";
        button.textContent="Добавить карточку";
        closeBtn.addEventListener("click",this.closeApp.bind(this));
        button.addEventListener("click",this.addTask.bind(this));
        this.elem.append(button);

    }

    closeApp(e){
        let btn = e.target.nextSibling;
        let textArea =btn.nextSibling;
        let taskEdit =document.querySelector(".task-edit") ;
        btn.classList.remove("active");
        textArea.classList.remove("active");
        e.target.classList.remove("active");
        taskEdit.classList.add("active");
    }

    addTask(e){
        let textArea = e.target.previousSibling;
        if(textArea.value){
            let newTask = document.createElement("p");
            newTask.className='task';
            newTask.textContent=textArea.value;
            newTask.draggable='true';
            newTask.addEventListener("dragstart",this.dragStart.bind(this));
            this.elem.append(newTask)
        }
    }

    dragStart(e){
        this.drugElem = e.target;
    }

    dragOver(e){
        e.preventDefault();
    }

    drop(e){
        e.preventDefault();
        if(e.target.classList.contains("task")) {
            e.target.after(this.drugElem);

        }
        else {
            this.elem.append(this.drugElem);
        }
        this.drugElem = null;
    }
}

const TrelloV2 = new Trello("trello","trello-id");

TrelloV2.render();
