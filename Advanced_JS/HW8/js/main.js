const btn = document.getElementById("btn");
btn.addEventListener("click",getLocation);
async function getLocation(){
    const {data:request} = await axios.get("https://api.ipify.org/?format=json")
    const url = `http://ip-api.com/json/${request.ip}?fields=continent,country,regionName,city,zip,district&lang=ru`
    const {data:ipRequest} =  await axios.get(url)
    const html = `<ul>
                    <li>Континент: ${ipRequest.continent}</li>
                    <li>Страна: ${ipRequest.country}</li>
                    <li>Регион: ${ipRequest.regionName}</li>
                    <li>Город: ${ipRequest.city}</li>
                    <li>Почтовый код: ${ipRequest.zip}</li>
                    </ul>`
    this.insertAdjacentHTML("afterend",html)
}
