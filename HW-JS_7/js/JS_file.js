let array = ['Apple', 'Samsung', ['HTC', 'Nokia'], ['LG','Siemens'], 'Huawei', 'Xiaomi'];
let timer = document.getElementById('timer');
timer.innerHTML = '10 seconds';
let counter = 10;

function createList(arr) {
    let ul = document.createElement('ul');
    document.getElementById('container').appendChild(ul);
    arr.map(function (elem) {
        if (typeof elem == 'object') {
            let liIn = document.createElement('li');
            ul.appendChild(liIn);
            let ulIn = document.createElement('ul');
            liIn.appendChild(ulIn);
            elem.map(function (elemIn) {
                let li = document.createElement('li');
                ulIn.appendChild(li);
                li.innerHTML = li.innerHTML + `Non Popular Phones: ${elemIn}`;
            })
        } else {
            let li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = li.innerHTML + `Popular Phones: ${elem}`;
        }
    });
    setInterval(function () {
        counter--;
        if (counter < 0) {
            document.body.innerHTML = '';
        } else {
            timer.innerHTML = counter.toString() + ' seconds';
        }
    }, 1000);
}
createList(array);
