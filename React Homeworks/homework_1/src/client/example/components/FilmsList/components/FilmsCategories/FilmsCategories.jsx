import React from 'react';
import "./style.css"

export const FilmsCategories = () => {
    return(
        <ul className="films-categories-menu">
            <li className="films-categories-item active">Trending</li>
            <li className="films-categories-item">Top Rated</li>
            <li className="films-categories-item">New Arrivals</li>
            <li className="films-categories-item">Trailers</li>
            <li className="films-categories-item">Coming Soon</li>
            <li className="films-categories-item">Genre</li>
        </ul>
    )
}
