import React from "react"
import "./style.css"

export const AboutUs = () => {
    return(
        <ul className="about_us_list">
          <li className="about_us_item">About</li>
          <li className="about_us_item">Terms of Service</li>
          <li className="about_us_item">Contact</li>
        </ul>
    )
}