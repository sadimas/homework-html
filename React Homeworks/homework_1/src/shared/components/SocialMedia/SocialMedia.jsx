import React from 'react';
import "./style.css"
import youtube from "./img/youtube.png"

export const SocialMedia = ()=> {
    return(
        <div className="social-media-container">
            <i className="fa fa-facebook-f media-logo"></i>
            <i className="fa fa-twitter media-logo"></i>
            <i className="fa fa-pinterest-p media-logo"></i>
            <i className="fa fa-instagram media-logo"></i>
            <img src={`${youtube}`} alt="#" className="media-logo"/>
        </div>
    )
}
