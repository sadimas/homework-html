import React from 'react';
import './App.css';
import './bootstrap.min.css';
import AppHeader from "./client/components/AppHeader";
import MainContent from "./client/components/AppBody/components/MainContent";

function App() {
    return (
        <div className="container mt-4">
        <AppHeader/>
        <MainContent/>
        </div>
)};

export default App;

