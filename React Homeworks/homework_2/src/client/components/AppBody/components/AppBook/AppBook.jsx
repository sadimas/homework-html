import React, {Component} from 'react';
import BookForm from "../BookForm";
import BookCount from "../BookCount";
import BookList from "../BookList";



class AppBook extends Component {

    state = {
        bookList: []
    };

    addBook = (title, author, isbn,) => {
        const newBook = {title, author, isbn};
        this.setState(({bookList}) => {
            return {
                bookList: [...bookList, newBook]
            }
        })
    };

    dltBook = (index) => {
        this.setState(({bookList}) => {
            const newBookList = [...bookList];
            newBookList.splice(index, 1);
            return {
                bookList: newBookList
            }
        })
    };
    editBook = (index, title, author, isbn) => {
        this.setState(({bookList}) => {
            const newBookList = [...bookList];
            newBookList[index] = {title, author, isbn};
            return {
                bookList: newBookList
            }
        })
    };

    render() {
        const {bookList} = this.state;
        const count = bookList.length;
        const {addBook, dltBook, editBook} = this;
        return (
            <>
                <div className="row">
                    <div className="col-lg-4">
                        <BookForm addBook={addBook}/>
                    </div>
                </div>
                <BookCount count={count}/>
                <BookList bookList={bookList} dltBook={dltBook} editBook={editBook}/>
            </>
        )
    }
}

export default AppBook;
