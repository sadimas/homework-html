import React from 'react';
import BookItem from "../BookItem";

    const BookList = ({bookList, dltBook, editBook}) => {
    const bookItems = bookList.map((book, index) =>
            <BookItem {...book}
                      dltBook={() => dltBook(index)}
                      editBook={(title, author, isbn) => editBook(index, title, author, isbn)}
        />);
    return (
        <table className="table table-striped mt-2">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>ISBN#</th>
            </tr>
            </thead>
            <tbody>
            {bookItems}
            </tbody>
        </table>
    )
};

export default BookList
