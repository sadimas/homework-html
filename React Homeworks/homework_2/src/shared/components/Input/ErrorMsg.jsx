import React from "react";

export const ErrorMsg = () => {
    return (
        <div className="text-danger font-italic">
            Это поле обязательно к заполнению!
        </div>
    )
};

