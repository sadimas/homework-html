import React from 'react'

const Input = (props) => {
    const {type = "text", value, name, placeholder, className, handleChange} = props;
    const fullClassName = `form-control ${className || ""}`;
        return <input
        name = {name} placeholder = {placeholder}
        type = {type} value = {value}
        className = {fullClassName} onChange = {handleChange}
        />};

export default Input;
