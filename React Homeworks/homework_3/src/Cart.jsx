import React, {useReducer} from "react";



export default function Cart ({cart, removeFromCart}) {

    let totalPrice = cart.reduce((acc, curr) => acc + curr.cost, 0);
        const [count,  dispatch] = useReducer((state, action) => {
        switch (action) {
            case "-":
                return state - 1;
            case "+":
                return state + 1;
            case "clearCart":
                return cart = [];

            default:
                return state
        }},(cart.length));


        return (
        <>
            <h1>Cart {count} </h1>
            <h1>Total price {totalPrice} </h1>
            <button onClick = {() => dispatch("clearCart")}>Clear</button>
            <div className="products">
                 {cart.map((product, index) => (
                     <div className="product" key={index}>
                         <h3>{product.name}</h3>
                         <h4>{product.cost}</h4>
                         <button onClick = {() => dispatch("-")}>-</button>
                         <button onClick={() => dispatch("+")}>+</button>
                         <img src={product.image} alt={product.name}/>
                         <button onClick ={() => removeFromCart(product)}>
                             Remove
                         </button>
                     </div>
                 ))}
            </div>
            </>
    )
    }



