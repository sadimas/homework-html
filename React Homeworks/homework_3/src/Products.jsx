import React, {useState} from "react";

export default function Products ({addToCart}) {
    const [products] = useState([
        {
            name: 'Snickers',
            cost: 24,
            image: 'https://images.heb.com/is/image/HEBGrocery/000121400'
        },{
            name: 'Bounty',
            cost: 16,
            image: 'https://img.fozzyshop.com.ua/110086-large_default/batonchik-bounty-s-myakotyu-kokosa-v-molochnom-shokolade.jpg'
        },{
            name: 'Twix',
            cost: 12,
            image: 'https://images.heb.com/is/image/HEBGrocery/000121394'
        },{
            name: 'Mars',
            cost: 18,
            image: 'https://citymarteg.com/image/cache/catalog/cat/chocolate/Galaxy/Mars%20Chocolate%2051%20Gm-550x550.jpeg'
        },


    ]);

    return (
        <>
            <h1>Products</h1>
            <div className="products">
                {products.map((product, index) => (
                    <div className="product" key={index}>
                        <h3>{product.name}</h3>
                        <h4>{product.cost} грн.</h4>
                        <img src={product.image} alt={product.name}/>
                        <button onClick ={() => addToCart(product)}>
                            Add to Cart
                        </button>
                    </div>
                ))}
            </div>
        </>
    )
}