import React,{useReducer,useEffect,useState} from 'react'
import {timeReducer} from './timer_reducer'
import './App.scss';


function App() {
    const initialState = {
        isTimerStarted: false,
        seconds: 0,
        tens: 0
    };
    const [state,dispatch] = useReducer(timeReducer,initialState);
    const [intervalStorage,setIntervalStorage] = useState(null);

    useEffect(()=>{
        if (state.isTimerStarted){
            setIntervalStorage(setInterval(()=> dispatch({type: "ADD_TIME"}),10))
        } else {
            clearInterval(intervalStorage)
        }
    },[state.isTimerStarted]);

    const startTimer = () => dispatch({type: "START_TIMER"});
    const stopTimer = () => dispatch({type: "STOP_TIMER"});
    const resetTimer = () => dispatch({type: "RESET_TIMER"});

    const seconds = state.seconds < 10 ? `0${state.seconds}` : `${state.seconds}`;
    const tens = state.tens < 10 ? `0${state.tens}` : `${state.tens}`;

    return (
        <div className="wrapper">
        <h1>Stopwatch</h1>
        <h2>Let's fun!</h2>
    <p><span id="seconds">{seconds}</span>:<span id="tens">{tens}</span></p>
    <button id="button-start" onClick={()=>startTimer()}>Start</button>
    <button id="button-stop" onClick={()=>stopTimer()}>Stop</button>
    <button id="button-reset" onClick={()=>resetTimer()}>Reset</button>
    </div>
);
}


export default App;

