export const timeReducer = (state, action) => {
    switch (action.type) {
        case "START_TIMER":
            return {...state,isTimerStarted: true};
        case "STOP_TIMER":
            return {...state,isTimerStarted: false};
        case "RESET_TIMER":
            return {...state,isTimerStarted: false,seconds:0,tens:0};
        case "ADD_TIME":
            if (state.tens >= 99){
                state.seconds++;
                return {...state,tens: 0}
            } else{
                state.tens++;
                return{...state}
            }
        default:
            break;
    }
}
